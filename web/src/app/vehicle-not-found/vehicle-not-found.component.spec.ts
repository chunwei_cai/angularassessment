import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleNotFoundComponent } from './vehicle-not-found.component';

describe('VehicleNotFoundComponent', () => {
  let component: VehicleNotFoundComponent;
  let fixture: ComponentFixture<VehicleNotFoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleNotFoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
